import socket, select, string, sys
import sqlite3 as lite

if __name__ == "__main__":
    if(len(sys.argv) < 3):
        print "Usage: python getdata.py hostname port"
        sys.exit()

    host = sys.argv[1]
    port = int(sys.argv[2])

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        s.connect((host, port))
    except:
        print "Unable to connect"
        sys.exit()

    print "Connected to remote host"

    con = lite.connect('test1.db')
    with con:
        cur = con.cursor()

        data = s.makefile()
        line = data.readline()

        while line:
            print line
            cur.execute(line)
            print "executed statement " + line

            if "select" in line:
                print "Found select in query statement"
                for row in cur.execute(line):
                    print row
                    s.send(str(row))

            line = data.readline()
