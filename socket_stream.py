import socket
import sys

HOST = ''
PORT = 8888

host = 'www.google.com'
port = 80

#Socket Creation
try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error, msg:
    print 'Failed to create socket. Error code: ' + str(msg[0]) + ', Error message: ' + msg[1]
    sys.exit();

print 'Socket Created'

#Socket Binding
try:
    s.bind((HOST, PORT))
except socket.error, msg:
    print 'Bind failed. Error code: ' + str(msg[0]) + ' Message ' + msg[1]
    sys.exit()
print 'Socket bound'

#Get Hostname
try:
    remote_ip = socket.gethostbyname( host )
except socket.gaierror:
    print 'Hostname could not be resolved. Exiting'
    sys.exit()
print 'IP address of ' + host + ' is ' + remote_ip

#Connect to remote server
s.connect((remote_ip, port))
print 'Socket connected to ' + host + ' on IP ' + remote_ip

message = "GET / HTTP/1.1\r\n\r\n"

#Talk to server
try:
    s.sendall(message)
except socket.error:
    print 'Send failed'
    sys.exit()
print 'Message sent successfully'

reply = s.recv(4096)
#print reply
