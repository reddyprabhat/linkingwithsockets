import socket, select, string, sys
import sqlite3 as lite

if __name__ == "__main__":
    if(len(sys.argv) < 3):
        print "Usage: python telnet.py hostname port"
        sys.exit()

    host = sys.argv[1]
    port = int(sys.argv[2])

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        s.connect((host, port))
    except:
        print "Unable to connect"
        sys.exit()
    print "Connected to remote host"

    con = lite.connect('test1.db')
    with con:
        cur = con.cursor()

        if 1:
            data = s.makefile()
            line = data.readline()
            columns = line.split(',')
            print columns
            cur.execute("drop table if exists testdata;")
            stmt = "create table testdata(" + columns[0] + "," + columns[1] + "," + columns[2] + ");"
            print stmt
            cur.execute(stmt)
            line = data.readline()
            while line:
                print line
                values = line.split(',')
                stmt = "insert into testdata values('" + values[0] + "', '" +  values[1] + "', '" + values[2] + "');"
                print stmt
                cur.execute(stmt)
                print "executed statement" + stmt
                line = data.readline()

