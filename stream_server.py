import socket, select
import sys

def broadcast_data(sock, file):
    for socket in CONNECTION_LIST:
        line = file.readline()
    
        if socket != server_socket and socket != sock:
            while line:
                try:
                    socket.send(line)
                    line = file.readline()
                except:
                    socket.close()
                    CONNECTION_LIST.remove(socket)
                    break
            socket.close()


if __name__ == "__main__":
    CONNECTION_LIST = []
    PORT = 5000

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(("0.0.0.0", PORT))
    server_socket.listen(10)

    CONNECTION_LIST.append(server_socket)
    print "Broadcasting data on port " + str(PORT)

    while 1:
        read_sockets, write_sockets, error_sockets = select.select(CONNECTION_LIST,[],[])

    for sock in read_sockets:
        if sock == server_socket:
            sockfd, addr = server_socket.accept()
            CONNECTION_LIST.append(sockfd)
            print "Connected to client" % addr

            broadcast_data(sockfd, open('data.csv','r'))
    server_socket.close()
