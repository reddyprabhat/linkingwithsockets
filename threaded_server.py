import socket
import sys
from _thread import *

HOST = ''
PORT = 8886


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


try:
    s.bind((HOST, PORT))
except socket.error as msg:
    print(str(msg))
    sys.exit()
print('Socket Bound')

s.listen(10)
print('Socket Listening')


def threaded_client(conn):
    stmt = "select * from inserteddata;"
    conn.send(str.encode(stmt))

    while True:
        data = conn.recv(4096)
        if not data:
            break
        print("Received data: " + data)
    conn.close()

while True:
    conn, addr = s.accept()
    print('Connected with ' + addr[0] + ':' + str(addr[1]))
    
    start_new_thread(threaded_client,(conn,))
