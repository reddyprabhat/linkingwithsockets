import socket
import sys

HOST = ''
PORT = 8883

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print 'Socket Created'

try:
    s.bind((HOST, PORT))
except socket.error, msg:
    print 'Bind failed. Error Code : ' + str(msg[0]) + ' Meassge ' + msg[1]
    sys.exit()
print 'Socket Bound'

s.listen(10)
print 'Socket Listening'

conn, addr = s.accept()

print 'Connected with ' + addr[0] + ':' + str(addr[1])

file = open('data2.csv','r')
line = file.readline()
while line:
    conn.sendall(line)
    line = file.readline()

#TODO: Run SQL query, loop through result set
#      write one record at a time to client

conn.close()
s.close()
