import socket
import sys

HOST = ''
PORT = 8886

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print 'Socket Created'

try:
    s.bind((HOST, PORT))
except socket.error, msg:
    print 'Bind failed. Error Code : ' + str(msg[0]) + ' Meassge ' + msg[1]
    sys.exit()
print 'Socket Bound'

s.listen(10)
print 'Socket Listening'

conn, addr = s.accept()

print 'Connected with ' + addr[0] + ':' + str(addr[1])

query1 = 'drop table if exists inserteddata;\n'
query2 = 'create table inserteddata(col1, col2, col3, col4);\n'
query3 = 'insert into inserteddata values (1, "first", 10, 1.0);\n'
query4 = 'insert into inserteddata values (2, "second", 20, 2.0);\n'
query5 = 'insert into inserteddata values (3, "third", 30, 3.0);\n'
query6 = 'insert into inserteddata values (4, "fourth", 40, 4.0);\n'
query7 = 'insert into inserteddata values (5, "fifth", 50, 5.0);\n'
query8 = 'select * from inserteddata;'

conn.sendall(query1)
conn.sendall(query2)
conn.sendall(query3)
conn.sendall(query4)
conn.sendall(query5)
conn.sendall(query6)
conn.sendall(query7)
conn.sendall(query8)

data = s.recv(4096)

line = data.readline()
print line
while line:
    print line
    line = data.readline()

conn.close()
s.close()
